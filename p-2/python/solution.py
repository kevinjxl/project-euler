#!/usr/bin/env python

def main():
    print "start to process main"
    pre = 0
    current = 1
    result = 0
    while current < 4000000:
        nt = pre + current
        if nt % 2 == 0:
            result = result + nt
        pre = current
        current = nt
    print result 

if __name__ == "__main__":
    main()
