#!/usr/bin/env python

def main():
    a = 1
    b = 1
    c = a + b
    result = 0
    while c < 4000000:
        result = result + c
        a = b + c
        b = c + a
        c = a + b
    print result

if __name__ == "__main__":
    main()
