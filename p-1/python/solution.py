#!/usr/bin/env python

def mult_of_3_or_5(limit):
    return sum([x for x in range(1, limit) if x % 3 == 0 or x % 5 == 0])

def main():
    print mult_of_3_or_5(1000)

if __name__ == "__main__":
    main()
