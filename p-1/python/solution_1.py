#!/usr/bin/env python

def sum_divisible_by(num, limit):
    n = limit / num
    return num * ((1 + n) * n / 2)

def main():
    print (sum_divisible_by(3, 999) + sum_divisible_by(5, 999) - sum_divisible_by(15, 999))

if __name__ == "__main__":
    main()
