#!/usr/bin/env python

import sys

INPUT_NUM = 600851475143

def find_min_factor(num):
    d = 2
    while d <= num:
        if num % d == 0:
            return d
        d = d + 1

def main(args):
    max_pf = 0
    num = INPUT_NUM
    temp = -1
    while num > 1:
        temp = find_min_factor(num)
        if is_prime(temp) and temp > max_pf:
            max_pf = temp
        num = num / temp
    
    print find_min_factor(max_pf)

if __name__ == "__main__":
    main(sys.argv)
